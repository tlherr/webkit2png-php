<?php
namespace tlherr;
use Symfony\Component\Process\Process;

/**
 * PHP wrapper class for the webkit2png application on OS X.
 *
 * @author Kevin Jung
 *
 */
class webkit2png {
    /**
     * Holds the options and contains the default directory path.
     * @var array
     */
    private $options = array();
    private $url;

    /**
     * Set flags vlaues for the options.
     * @var array
     */
    private $flags = array(
        'url'            => null,
        'fullsize'       => '-F',
        'width'          => '-W',
        'height'         => '-H',
        'zoom'           => '-z',
        'thumb'          => '-T',
        'clipped'        => '-C',
        'clipped-width'  => '--clipwidth',
        'clipped-height' => '--clipheight',
        'scale'          => '-s',
        'dir'            => '-D',
        'filename'       => '-o',
        'md5'            => '-m',
        'datestamp'      => '-d',
        'delay'          => '--delay',
        'js'             => '--js',
        'no-images'      => '--no-images',
        'no-js'          => '--no-js',
        'transparent'    => '--transparent',
        'user-agent'     => '--user-agent',
    );

    /**
     * Holds the query to be executed.
     * @var string
     */
    private $query = '/usr/local/bin/webkit2png ';

    /**
     * Initialize the class
     */
    public function __construct()
    {
        // Set the environment path so you have access to webkit2png within PHP.
        // If you installed webkit2png via homebrew, include the following path.
        putenv("PATH=" . $_ENV["PATH"] .':/usr/local/bin');
        $webkit2png = trim(shell_exec('type -P webkit2png'));

        try
        {	if (empty($webkit2png)){
            throw new \Exception('Unable to find webkit2png. Please check your environment paths to ensure that PHP has access to the webkit2png binary.');
        }
        }

        catch (\Exception $e)
        {
            die($e->getMessage());
        }
    }

    /**
     * Set the $url variable
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Set the $options variable
     * @param array $options Provided options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * Generate the image(s)
     */
    public function getImage()
    {
        $this->setQuery();
        /**
         * @var $process \Symfony\Component\Process\Process
         */
        $process = new Process($this->getQuery());
        $process->start();

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > '.$buffer;
            } else {
                echo 'OUT > '.$buffer;
            }
        });
    }

    /**
     * Generate and return the created query
     */
    public function getQuery()
    {
        $this->setQuery();
        return trim($this->query);
    }

    /**
     * Set the query based on the provided URL and options
     */
    private function setQuery()
    {
        $this->query = '/usr/local/bin/webkit2png ';
        array_walk($this->options, function(&$value){
            if ($value === true) {
                $value = null;
            }
        });

        //This is where we want to take whatever user entered under setOptions and merge it with existing flags
        foreach($this->options as $key => $option) {
            $this->query .= $this->flags[$key].' '.$option.' ';
        }
        //        >& \dev\null . ' 2>&1'
        $this->query .= $this->url;

    }

}