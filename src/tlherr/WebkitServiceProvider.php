<?php

namespace tlherr;

use Silex\Application;
use Silex\ServiceProviderInterface;

class WebkitServiceProvider implements ServiceProviderInterface {
    public function register(Application $app) {
        $app['tlherr.webkit2png'] =  new webkit2png();
        return $app;
    }

    public function boot(Application $app) {}
}
